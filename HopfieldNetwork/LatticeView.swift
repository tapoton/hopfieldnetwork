//
//  LatticeView.swift
//  HopfieldNetwork
//
//  Created by Anton Tyutin on 06.12.16.
//  Copyright © 2016 Anton Tyutin. All rights reserved.
//

import UIKit

protocol LatticeViewDelegate: class {
    
    func latticeView(_ view: LatticeView, shouldFillCellAt position: LatticeView.Position) -> Bool
    
    func latticeView(_ view: LatticeView, didTouchCellAt position: LatticeView.Position)
}

@IBDesignable
class LatticeView: UIView {
    
    struct Position {
        var row, column: Int
    }
    
    struct Resolution {
        var width, height: Int
        
        static var zero = Resolution(width: 0, height: 0)
        static var one = Resolution(width: 1, height: 1)
    }
    
    @IBInspectable
    var latticeWidth: Int {
        get {
            return resolution.width
        }
        set {
            resolution.width = newValue
        }
    }
    
    @IBInspectable
    var latticeHeight: Int {
        get {
            return resolution.height
        }
        set {
            resolution.height = newValue
        }
    }
    
    var resolution: Resolution = .one {
        didSet {
            setNeedsDisplay()
        }
    }
    
    weak var delegate: LatticeViewDelegate?
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        let xStep = rect.width / CGFloat(resolution.width)
        let yStep = rect.height / CGFloat(resolution.height)
        
        for i in 0...resolution.width {
            
            let x = (xStep * CGFloat(i)).rounded()
            
            context.move(to: CGPoint(x: x, y: 0))
            context.addLine(to: CGPoint(x: x, y: rect.height))
        }
        
        for i in 0...resolution.height {
            
            let y = (yStep * CGFloat(i)).rounded()
            
            context.move(to: CGPoint(x: 0, y: y))
            context.addLine(to: CGPoint(x: rect.width, y: y))
        }
        
        context.setLineWidth(1 / UIScreen.main.scale)
        context.setStrokeColor(UIColor.black.cgColor)
        context.strokePath()
        
        context.setStrokeColor(UIColor.black.cgColor)
        
        for row in 0..<resolution.height {
            for column in 0..<resolution.width {
                
                guard delegate?.latticeView(self, shouldFillCellAt: Position(row: row, column: column)) == true else {
                    continue
                }
                
                let x = xStep * CGFloat(column)
                let y = yStep * CGFloat(row)
                
                let rect = CGRect(x: x,
                                  y: y,
                                  width: xStep,
                                  height: yStep)
                
                context.fill(rect)
            }
        }
    }
    
    private var currentPosition: Position?
    
    private func position(for point: CGPoint) -> Position {
        
        let xStep = bounds.width / CGFloat(resolution.width)
        let yStep = bounds.height / CGFloat(resolution.height)
        
        let row = Int(point.y / yStep)
        let column = Int(point.x / xStep)
        
        return Position(row: row, column: column)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let location = touches.first?.location(in: self) else {
            return
        }
        
        let position = self.position(for: location)
        
        currentPosition = position
        
        delegate?.latticeView(self, didTouchCellAt: position)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let location = touches.first?.location(in: self), self.bounds.contains(location) else {
            return
        }
        
        let position = self.position(for: location)
        
        guard position != currentPosition else {
            return
        }
        
        currentPosition = position
        
        delegate?.latticeView(self, didTouchCellAt: position)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        currentPosition = nil
    }
}

extension LatticeView.Position: Equatable {
    
    static func ==(p1: LatticeView.Position, p2: LatticeView.Position) -> Bool {
        return p1.row == p2.row && p1.column == p2.column
    }
}
