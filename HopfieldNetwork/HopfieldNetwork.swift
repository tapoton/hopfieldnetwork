//
//  HopfieldNetwork.swift
//  HopfieldNetwork
//
//  Created by Anton Tyutin on 06.12.16.
//  Copyright © 2016 Anton Tyutin. All rights reserved.
//

import Foundation

class HopfieldNetwork {
    
    var w: Matrix<Double>
    
    typealias ActivationFunction = (Double) -> Int
    var f: ActivationFunction
    
    init(size: Int, f: @escaping ActivationFunction) {
        w = Matrix(width: size,
                   height: size,
                   defaultValue: 0.0)
        
        self.f = f
    }
    
    func learn(_ input: [Int]) {
        assert(input.count == w.height)
        
        for i in 0..<w.width {
            for j in 0..<w.height where i != j {
                w[i, j] += Double(input[i] * input[j])
            }
        }
    }
    
    func guess(for input: [Int],
               intermediateResult: ([Int]) -> Void) -> [Int] {
        
        var y = input
        
        while true {
            
//            let width = Int(sqrt(Double(input.count)))
            
            var y1: [Int] = []
            
            for j in (0..<w.width) {
                
                var s = 0.0
                
                for i in (0..<y.count) {
                    s += w[i, j] * Double(y[i])
                }
                
//                print(s, separator: "", terminator: " ")
//                
//                if j > 0 && j % width == 0 {
//                    print()
//                }
                
                y1.append(f(s))
            }
            
            print()
            
            if y1 == y {
                break
            }
            
            intermediateResult(y1)
            
            y = y1
        }
        
        return y
    }
    
    func forget(_ input: [Int]) {
        for i in 0..<w.height {
            for j in 0..<w.width {
                w[i, j] -= Double(input[i] * input[j]) * 0.1
            }
        }
    }
}
