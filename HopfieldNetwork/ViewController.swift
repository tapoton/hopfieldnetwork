//
//  ViewController.swift
//  HopfieldNetwork
//
//  Created by Anton Tyutin on 06.12.16.
//  Copyright © 2016 Anton Tyutin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var latticeView: LatticeView!
    
    struct Constants {
        static let width = 40
        static let height = 40
    }
    
    
    var pictureMatrix = Matrix(width: Constants.width,
                               height: Constants.height,
                               defaultValue: -1)
    
    var network = HopfieldNetwork(size: Constants.width * Constants.height,
                                  f: { $0 >= 0 ? 1 : -1 })
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        network.learn(Pictures.pacman1.map { $0 == 1 ? 1 : -1 })
        network.learn(Pictures.mario.map { $0 == 1 ? 1 : -1 })
        network.learn(Pictures.sonic.map { $0 == 1 ? 1 : -1 })
        
        latticeView.delegate = self
        latticeView.resolution = LatticeView.Resolution(width: Constants.width,
                                                        height: Constants.height)
        
    }
    
    var lastOutput: [Int]?
    
    @IBAction
    func guess() {
        
        printMatrix()
        
        DispatchQueue.global().async {
            
            self.lastOutput = self.network.guess(
                for: self.pictureMatrix.plain,
                intermediateResult: { result in
                    
                    self.pictureMatrix.plain = result
                    
                    self.printMatrix()
                    
                    
                    DispatchQueue.main.async {
                        self.latticeView.setNeedsDisplay()
                    }
                    
                    Thread.sleep(forTimeInterval: 1)
                }
            )
        }
    }
    
    func printMatrix() {
        for i in 0..<pictureMatrix.height {
            for j in 0..<pictureMatrix.width {
                
                print(pictureMatrix[i, j] == 1 ? "1" : " ",
                      separator: "",
                      terminator: ",")
            }
            
            print()
        }
        
        print()
    }
    
    @IBAction
    func forget() {
        
        if let input = self.lastOutput {
            network.forget(input)
        }
        
    }
    
    @IBAction
    func selectImage(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            pictureMatrix.plain = Pictures.pacman1.map { $0 == 1 ? 1 : -1 }
        case 1:
            pictureMatrix.plain = Pictures.mario.map { $0 == 1 ? 1 : -1 }
        case 2:
            pictureMatrix.plain = Pictures.sonic.map { $0 == 1 ? 1 : -1 }
        default:
            break
        }
        
        latticeView.setNeedsDisplay()
    }
    
    var draw = false
    
    @IBAction
    func changeDrawingMode(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            draw = false
        case 1:
            draw = true
        default:
            break
        }
    }
}

extension ViewController: LatticeViewDelegate {
    
    func latticeView(_ view: LatticeView, shouldFillCellAt position: LatticeView.Position) -> Bool {
        return pictureMatrix[position.row, position.column] == 1
    }
    
    func latticeView(_ view: LatticeView, didTouchCellAt position: LatticeView.Position) {
        
//        pictureMatrix[position.row, position.column] = -pictureMatrix[position.row, position.column]
        pictureMatrix[position.row, position.column] = draw ? 1 : -1
        
        if position.row + 1 < latticeView.resolution.height {
            pictureMatrix[position.row + 1, position.column] = draw ? 1 : -1
        }
        if position.row > 0 {
            pictureMatrix[position.row - 1, position.column] = draw ? 1 : -1
        }
        
        if position.column + 1 < latticeView.resolution.width {
            pictureMatrix[position.row, position.column + 1] = draw ? 1 : -1
        }
        if position.column > 0 {
            pictureMatrix[position.row, position.column - 1] = draw ? 1 : -1
        }
        
        latticeView.setNeedsDisplay()
    }
}
