//
//  Matrix.swift
//  HopfieldNetwork
//
//  Created by Anton Tyutin on 06.12.16.
//  Copyright © 2016 Anton Tyutin. All rights reserved.
//

struct Matrix<T> {
    
    var width, height: Int
    
    var plain: [T]
    
    init(width: Int, height: Int, constructValue: (Int, Int) -> T) {
        self.width = width
        self.height = height
        
        plain = (0..<width).flatMap { column in
            return (0..<height).map { row in
                return constructValue(row, column)
            }
        }
    }
    
    init(width: Int, height: Int, defaultValue: T) {
        self.init(
            width: width,
            height: height,
            constructValue: { row, column in
                return defaultValue
            }
        )
    }
    
    subscript(row: Int, column: Int) -> T {
        get {
            return plain[width * row + column]
        }
        
        set {
            plain[width * row + column] = newValue
        }
    }
}


extension Matrix: CustomStringConvertible {
    
    var description: String {
        
        var s = ""
        
        for row in 0..<height {
            for column in 0..<width {
                s += "\(self[row, column])"
            }
            
            s += "\n"
        }
        
        return s
    }
}
